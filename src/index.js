import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import items from './reducers/itemsReducer';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';

let reducers = combineReducers(
  {
    items
  }
)
 
let store = createStore(
  reducers,
  compose(
    applyMiddleware(thunkMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

ReactDOM.render(<Provider store={store}>
        <App />
  </Provider>, document.getElementById('root'));
registerServiceWorker();
