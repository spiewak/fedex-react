const initialState = [
        {
            id: 1,
            task:"jeden"
        }, {
            id: 2,
            task:"dwa"
        }, {
            id: 3,
            task:"trzy"
        }
];

const itemsReducer = (state=initialState, action) => {
	switch(action.type) {
        case 'ADD_TASK': {
            return [...state, action.payload];
        }
        case 'REMOVE_TASK': {
            return state.filter(item => item.id !== action.payload);
        }
		default:
			return state;
	}
}

export default itemsReducer;
