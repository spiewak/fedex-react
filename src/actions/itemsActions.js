

export function addTask(){
    return {
        type: 'ADD_TASK',
        payload: {
            id: Math.floor(Math.random()*1000) + 100,
            task : 'Sample Task'
        }
    }
}

export function removeTask(id){
    return {
        type: 'REMOVE_TASK',
        payload: id
    }
}