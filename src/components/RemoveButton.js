import React from 'react';

export const RemoveButton = ({id, onTaskRemove}) => {
        const taskRemove = () => {
            onTaskRemove(id);
        }
        return (
            <button onClick={taskRemove}>Usuń taska</button>
        )
}

export default RemoveButton;