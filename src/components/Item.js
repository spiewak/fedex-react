import React from 'react';
import RemoveButton from './RemoveButton';
export const Item = ({task, taskId, onTaskRemove}) => {
    return (
        <h1>
            {task}
            <RemoveButton id={taskId} onTaskRemove={onTaskRemove} />
        </h1>
    )
}

export default Item;