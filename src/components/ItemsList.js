import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import { connect } from 'react-redux';

import { addTask, removeTask } from './../actions/itemsActions';
import { bindActionCreators } from 'redux';

export class ItemsList extends React.Component {
    constructor() {
        super();
    }
 
    render() {
        return (
            <div>
                <button onClick={this.props.actions.addTask}>Add Task </button>
                
                {
                this.props.items.map((item) => {
                    return <Item task={item.task} 
                                taskId={item.id} 
                                onTaskRemove={this.props.actions.removeTask}
                                key={item.id}
                                 />
                })
            }
            </div>
        )
    }
}


ItemsList.propTypes = {
    items : PropTypes.array.isRequired,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators( { addTask, removeTask }, dispatch) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);