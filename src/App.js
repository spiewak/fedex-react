import React, { Component } from 'react';
import ItemsList from './components/ItemsList';
import './App.css';

class App extends Component {
  render() {
    return (
        <ItemsList />
    );
  }
}

export default App;
